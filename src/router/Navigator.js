import { createStackNavigator } from 'react-navigation'
import LandingScreen from '../components/screens/LandingScreen'
import LoginScreen from '../components/screens/LoginScreen'
import RegisterScreen from '../components/screens/RegisterScreen'

const AuthenticationNavigator = createStackNavigator(
	{
		Landing: LandingScreen,
		Login: LoginScreen,
		Register: RegisterScreen
	},
	{
		initialRouteName: 'Landing'
	}
)

export default AuthenticationNavigator

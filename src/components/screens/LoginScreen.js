import React from 'react'
import {
	Container,
	Header,
	Left,
	Button,
	Icon,
	Body,
	Title,
	Right,
	Content,
	Form,
	Item,
	Label,
	Input,
	Text
} from 'native-base'

export default class LoginScreen extends React.Component {
	static navigationOptions = { header: null }
	constructor(props) {
		super(props)
		this.state = {
			email: '',
			password: ''
		}
	}
	render() {
		return (
			<Container>
				<Header>
					<Left>
						<Button
							transparent
							onPress={() => {
								this.props.navigation.goBack()
							}}
						>
							<Icon name="arrow-back" />
						</Button>
					</Left>
					<Body>
						<Title>Login</Title>
					</Body>
					<Right />
				</Header>
				<Content>
					<Form>
						<Item fixedLabel>
							<Label>Email</Label>
							<Input
								autoCapitalize="none"
								autoCorrect={false}
								keyboardType="email-address"
								placeholder="example@email.com"
								value={this.state.email}
								onChangeText={email => {
									this.setState({ email })
								}}
							/>
						</Item>
						<Item fixedLabel last>
							<Label>Password</Label>
							<Input
								autoCapitalize="none"
								autoCorrect={false}
								keyboardType="default"
								placeholder="securepassword"
								secureTextEntry={true}
								value={this.state.password}
								onChangeText={password => {
									this.setState({ password })
								}}
							/>
						</Item>
						<Button full primary>
							<Text>Login</Text>
						</Button>
						<Button full transparent danger>
							<Text>Forgot Password</Text>
						</Button>
					</Form>
				</Content>
			</Container>
		)
	}
}

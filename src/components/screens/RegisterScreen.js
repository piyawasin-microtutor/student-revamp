import React from 'react'
import {
	Container,
	Header,
	Left,
	Button,
	Icon,
	Body,
	Title,
	Right,
	Content,
	Form,
	Item,
	Label,
	Input,
	Text
} from 'native-base'

export default class RegisterScreen extends React.Component {
	static navigationOptions = { header: null }
	constructor(props) {
		super(props)
	}
	render() {
		return (
			<Container>
				<Header>
					<Left>
						<Button
							transparent
							onPress={() => {
								this.props.navigation.goBack()
							}}
						>
							<Icon name="arrow-back" />
						</Button>
					</Left>
					<Body>
						<Title>Register</Title>
					</Body>
					<Right />
				</Header>
				<Content>
					<Text>Register Screen</Text>
				</Content>
			</Container>
		)
	}
}

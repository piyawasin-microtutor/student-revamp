import React from 'react'
import {
	Container,
	Header,
	Left,
	Right,
	Body,
	Title,
	Content,
	Button,
	Text
} from 'native-base'

export default class LandingScreen extends React.Component {
	static navigationOptions = { header: null }
	constructor(props) {
		super(props)
	}
	render() {
		return (
			<Container>
				<Header>
					<Left />
					<Body>
						<Title>Welcome</Title>
					</Body>
					<Right />
				</Header>
				<Content>
					<Button
						full
						primary
						onPress={() => {
							this.props.navigation.navigate('Login')
						}}
					>
						<Text>Login</Text>
					</Button>
					<Button
						full
						dark
						onPress={() => {
							this.props.navigation.navigate('Register')
						}}
					>
						<Text>Register</Text>
					</Button>
				</Content>
			</Container>
		)
	}
}

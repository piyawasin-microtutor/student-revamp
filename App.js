import React from 'react'
import AuthenticationNavigator from './src/router/Navigator'

export default class App extends React.Component {
	constructor(props) {
		super(props)
	}
	render() {
		return <AuthenticationNavigator />
	}
}

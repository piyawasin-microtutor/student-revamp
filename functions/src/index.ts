import * as functions from 'firebase-functions'
import * as admin from 'firebase-admin'
admin.initializeApp()

const Firestore = require('@google-cloud/firestore')
const firestore = new Firestore()
const settings = { timestampsInSnapshots: true }
firestore.settings(settings)

const database = {}

export const students = functions.https.onRequest((request, response) => {
	firestore
		.collection('students')
		.get()
		.then(snapshot => {
			let data = {}
			snapshot.forEach(document => {
				data = { ...data, [document.id]: document.data() }
			})
			response.send(data)
		})
})

export const tutors = functions.https.onRequest((request, response) => {
	firestore
		.collection('tutors')
		.get()
		.then(snapshot => {
			let data = {}
			snapshot.forEach(document => {
				data = { ...data, [document.id]: document.data() }
			})
			response.send(data)
		})
})

export const studentsReviews = functions.https.onRequest(
	(request, response) => {
		firestore
			.collection('studentsReviews')
			.get()
			.then(snapshot => {
				let data = {}
				snapshot.forEach(document => {
					data = { ...data, [document.id]: document.data() }
				})
				response.send(data)
			})
	}
)

export const tutorsReviews = functions.https.onRequest((request, response) => {
	firestore
		.collection('tutorsReviews')
		.get()
		.then(snapshot => {
			let data = {}
			snapshot.forEach(document => {
				data = { ...data, [document.id]: document.data() }
			})
			response.send(data)
		})
})

export const lessons = functions.https.onRequest((request, response) => {
	firestore
		.collection('lessons')
		.get()
		.then(snapshot => {
			let data = {}
			snapshot.forEach(document => {
				data = { ...data, [document.id]: document.data() }
			})
			response.send(data)
		})
})

export const categories = functions.https.onRequest((request, response) => {
	firestore
		.collection('categories')
		.get()
		.then(snapshot => {
			let data = {}
			snapshot.forEach(document => {
				data = { ...data, [document.id]: document.data() }
			})
			response.send(data)
		})
})

export const onStudentsReviewCreate = functions.firestore
	.document('studentsReviews/{reviewId}')
	.onCreate((snapshot, context) => {
		const review = snapshot.data()
		const studentId = review.student.uid
		const studentRating = review.rating
		const studentDocumentRef = firestore.collection('students').doc(studentId)
		return firestore
			.runTransaction(transaction => {
				return transaction.get(studentDocumentRef).then(document => {
					if (!document.exists) {
						throw new Error('Document does not exist!')
					}
					const newRating = (document.data().rating + studentRating) / 2
					transaction.update(studentDocumentRef, { rating: newRating })
				})
			})
			.then(() => {
				console.log('Transaction successfully committed!')
			})
			.catch(error => {
				console.log('Transaction failed: ', error)
			})
	})

export const onTutorsReviewCreate = functions.firestore
	.document('tutorsReviews/{reviewId}')
	.onCreate((snapshot, context) => {
		const review = snapshot.data()
		const tutorId = review.tutor.uid
		const tutorRating = review.rating
		const tutorDocumentRef = firestore.collection('tutors').doc(tutorId)
		return firestore
			.runTransaction(transaction => {
				return transaction.get(tutorDocumentRef).then(document => {
					if (!document.exists) {
						throw new Error('Document does not exist!')
					}
					const newRating = (document.data().rating + tutorRating) / 2
					transaction.update(tutorDocumentRef, { rating: newRating })
				})
			})
			.then(() => {
				console.log('Transaction successfully committed!')
			})
			.catch(error => {
				console.log('Transaction failed: ', error)
			})
	})

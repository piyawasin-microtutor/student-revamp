"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const functions = require("firebase-functions");
const admin = require("firebase-admin");
admin.initializeApp();
const Firestore = require('@google-cloud/firestore');
const firestore = new Firestore();
const settings = { timestampsInSnapshots: true };
firestore.settings(settings);
const database = {};
exports.students = functions.https.onRequest((request, response) => {
    firestore
        .collection('students')
        .get()
        .then(snapshot => {
        let data = {};
        snapshot.forEach(document => {
            data = Object.assign({}, data, { [document.id]: document.data() });
        });
        response.send(data);
    });
});
exports.tutors = functions.https.onRequest((request, response) => {
    firestore
        .collection('tutors')
        .get()
        .then(snapshot => {
        let data = {};
        snapshot.forEach(document => {
            data = Object.assign({}, data, { [document.id]: document.data() });
        });
        response.send(data);
    });
});
exports.studentsReviews = functions.https.onRequest((request, response) => {
    firestore
        .collection('studentsReviews')
        .get()
        .then(snapshot => {
        let data = {};
        snapshot.forEach(document => {
            data = Object.assign({}, data, { [document.id]: document.data() });
        });
        response.send(data);
    });
});
exports.tutorsReviews = functions.https.onRequest((request, response) => {
    firestore
        .collection('tutorsReviews')
        .get()
        .then(snapshot => {
        let data = {};
        snapshot.forEach(document => {
            data = Object.assign({}, data, { [document.id]: document.data() });
        });
        response.send(data);
    });
});
exports.lessons = functions.https.onRequest((request, response) => {
    firestore
        .collection('lessons')
        .get()
        .then(snapshot => {
        let data = {};
        snapshot.forEach(document => {
            data = Object.assign({}, data, { [document.id]: document.data() });
        });
        response.send(data);
    });
});
exports.categories = functions.https.onRequest((request, response) => {
    firestore
        .collection('categories')
        .get()
        .then(snapshot => {
        let data = {};
        snapshot.forEach(document => {
            data = Object.assign({}, data, { [document.id]: document.data() });
        });
        response.send(data);
    });
});
exports.onStudentsReviewCreate = functions.firestore
    .document('studentsReviews/{reviewId}')
    .onCreate((snapshot, context) => {
    const review = snapshot.data();
    const studentId = review.student.uid;
    const studentRating = review.rating;
    const studentDocumentRef = firestore.collection('students').doc(studentId);
    return firestore
        .runTransaction(transaction => {
        return transaction.get(studentDocumentRef).then(document => {
            if (!document.exists) {
                throw new Error('Document does not exist!');
            }
            const newRating = (document.data().rating + studentRating) / 2;
            transaction.update(studentDocumentRef, { rating: newRating });
        });
    })
        .then(() => {
        console.log('Transaction successfully committed!');
    })
        .catch(error => {
        console.log('Transaction failed: ', error);
    });
});
exports.onTutorsReviewCreate = functions.firestore
    .document('tutorsReviews/{reviewId}')
    .onCreate((snapshot, context) => {
    const review = snapshot.data();
    const tutorId = review.tutor.uid;
    const tutorRating = review.rating;
    const tutorDocumentRef = firestore.collection('tutors').doc(tutorId);
    return firestore
        .runTransaction(transaction => {
        return transaction.get(tutorDocumentRef).then(document => {
            if (!document.exists) {
                throw new Error('Document does not exist!');
            }
            const newRating = (document.data().rating + tutorRating) / 2;
            transaction.update(tutorDocumentRef, { rating: newRating });
        });
    })
        .then(() => {
        console.log('Transaction successfully committed!');
    })
        .catch(error => {
        console.log('Transaction failed: ', error);
    });
});
//# sourceMappingURL=index.js.map